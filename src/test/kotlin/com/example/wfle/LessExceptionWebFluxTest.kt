package com.example.wfle

import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.Is.`is`
import org.junit.Test
import org.junit.experimental.theories.DataPoints
import org.junit.experimental.theories.Theory
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.HttpStatus
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner

@RunWith(SpringJUnit4ClassRunner::class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
class LessExceptionWebFluxTest{
   @Autowired
   lateinit var lessExceptionWebFlux : LessExceptionWebFlux

    companion object {
        @DataPoints
        @JvmStatic
        val status : List<Int> = HttpStatus
                .values()
                .filter { !it.is1xxInformational } //Hangs on 100's
                .map { it.value() }
    }

    @Test
    fun runAllCodes(){
        status
                .map {it to lessExceptionWebFlux.makeCall(it) }
                .map {it.first to it.second.block() }
                .forEach {assertThat(it.first, `is`(equalTo(it.second)))}
    }

    @Test
    fun badAddress(){
        val result = lessExceptionWebFlux
                .makebadAddressCall()
                .block()
        assertThat(result, `is`(0))
    }

}