package com.example.wfle

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class WebFluxWithLessExceptions

fun main(args: Array<String>) {
    runApplication<WebFluxWithLessExceptions>(*args)
}

