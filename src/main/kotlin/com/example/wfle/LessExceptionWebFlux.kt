package com.example.wfle

import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.client.WebClient

@Component
class LessExceptionWebFlux{
    val webClient : WebClient = WebClient.create("https://httpstat.us")

    fun makeCall(code : Int) =
            webClient
                .get()
                .uri("/$code")
                .exchange()
                .map { it.statusCode().value() }
                .doOnSuccess { println(it) }
                    .onErrorReturn(0)

    fun makebadAddressCall() =
            webClient
                    .get()
                    .uri("https://127.0.0.1:0")
                    .exchange()
                    .map { it.statusCode().value() }
                    .doOnSuccess { println(it) }
                    .onErrorReturn(0)
}